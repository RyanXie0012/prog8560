const Register = require("./models/registers");
const axios = require('axios');
const send = require('send');
const { find } = require("core-js/fn/array");
const { render } = require("ejs");
const { body } = require("express-validator");

let i;
let entryDate = [];

let employerDashboardRender = {
    ids : [],
    entryDates: [],
    regularHours: [], 
    overtimeHours: [],
    vacationHours: [],
    statutoryHours: [],
    sickHours: [],
    approve: [],
};
let editFile = {};

let info;
let jwtTokenVerify;
let userRole = "";
let loginError = "Please Enter the correct email or password."

const loginURL = 'http://www.thepayroll.ca/account/login';
const inputWorkingHoursURL = 'http://www.thepayroll.ca/api/PayrollEntry'
module.exports = function tabs(server) {
    // Setup home page
    server.get('/first', (req, res, next) => {
        res.render("first");
    });

    server.post('/first',async (req, res, next) => {     
        if(req.body.loginHint){
        //get email from loginHint
        //check mongodb
        //if exist        
        //  get jwt-token from mongodb
        //  render dashboard
        //if not exist
        //  render first
        //  in the post first, save jwt-token and login-hint
        }
        else{   
            axios.post(loginURL, {
                email: req.body.email,
                password: req.body.password
            })
            .then(function (response) {
                //Verify jwtToken to get payroll entry
                info = response.data;
                jwtTokenVerify = info.jwtToken;

                /*
                //Save data into mongodb
                const tokenInfo = new Register({
                    teamsemail://,
                    email: req.body.email,
                    password: req.body.password,
                    jwtTokens: jwtToken
                })
                tokenInfo.save();
                */

            if(info.roleNames == 'Employer')
                {                        
                    userRole = "Employer";

                    res.render('dashboard', {
                        userRole: userRole
                    });
                }
                else if(info.roleNames == 'Employee')
                {
                    userRole = "Employee";

                    res.render('dashboard', {
                        userRole: userRole
                    });
                }
                else{
                    res.render("first", {loginError: loginError});                
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    });

    server.post('/datefilter', (req, res, next)=>{
        axios.get(`http://www.thepayroll.ca/api/PayrollEntry/${req.body.year}/${req.body.week}/1/1`, {
            headers: {
                Authorization : `Bearer ${jwtTokenVerify}`
            },
        })
        .then(function(response){
            const entryInfo = response.data.entries;
            const LENGTH = entryInfo.length;            
            for (i = 0; i< LENGTH; i++) {
                employerDashboardRender.ids.push(entryInfo[i].employeeId);
                employerDashboardRender.entryDates.push(entryInfo[i].date.substring(0,19));
                employerDashboardRender.regularHours.push(entryInfo[i].regularHours);
                employerDashboardRender.overtimeHours.push(entryInfo[i].overtimeHours);
                employerDashboardRender.vacationHours.push(entryInfo[i].vacationHours);
                employerDashboardRender.statutoryHours.push(entryInfo[i].statutoryHours);
                employerDashboardRender.sickHours.push(entryInfo[i].sickHours);
                if(entryInfo[i].isApproved){
                    employerDashboardRender.approve.push("Yes");
                }
                else{
                    employerDashboardRender.approve.push("No");
                }                
            }
            //render dashboard for employer
            res.render("payrollentry", 
            {
                employerDashboardRender:employerDashboardRender, 
                userRole: userRole
            } );                   
            //Clear the array after excuting each search
            employerDashboardRender = {
                ids : [],
                entryDates: [],
                regularHours: [], 
                overtimeHours: [],
                vacationHours: [],
                statutoryHours: [],
                sickHours: [],
                approve: [],
            };
            console.log(entryDate); 
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    server.post('/workinghours', (req, res, next)=>{
        axios.post(inputWorkingHoursURL, 
            [{
                employeeId: req.body.employeeId,
                regularHours: req.body.regularHours,
                overtimeHours: req.body.overtimeHours,
                vacationHours: req.body.vacationHours,
                statutoryHours: req.body.statutoryHours,
                sickHours: req.body.sickHours,
                payPerHour: 0,
                date: req.body.date,
                isApproved: req.body.isApproved
            }],
            {
                headers: { Authorization : `Bearer ${jwtTokenVerify}`}
            })
        .then(function(response){
            if (response.data.id === 1){
                var success = "Successfully input working hours!"
                res.render("dashboard", 
                {
                    userRole: userRole,
                    info: success
                });
            }
        })
        .catch(function (error) {
            console.log(error);
        })
    });

    server.post('/edit', (req, res, next)=>{
        axios.post(inputWorkingHoursURL, 
            [{
                employeeId: req.body.employeeId,
                regularHours: req.body.regularHours,
                overtimeHours: req.body.overtimeHours,
                vacationHours: req.body.vacationHours,
                statutoryHours: req.body.statutoryHours,
                sickHours: req.body.sickHours,
                payPerHour: 0,
                date: req.body.date,
                "isApproved": req.body.isApproved
            }],
            {
                headers: { Authorization : `Bearer ${jwtTokenVerify}`}
            })
        .then(function(response){
            if (response.data.id === 1){
                var success = "Successfully change the payroll entry";
                console.log(req.body.isApproved); 
                res.render("datefilter", 
                {
                    userRole: userRole,
                    info: success
                });
            }
        })
        .catch(function (error) {
            console.log(error);
        })
    });
/*
    server.get('/dashboard', (req, res, next) => {
        var jwtToken;
        find({email: "m@a.co"}, function(err, verifyjwtToken){
            if(err){
                console.log(err);
            }
            else{
                jwtToken = verifyjwtToken;
                console.log(jwtToken);
            }
        });
        
 

        const info = userResponse.data;
        const jwtToken = info.jwtToken;
        axios.get(payrollEntryURL, {
            headers: {
                Authorization : `Bearer ${jwtToken}`
            },
        })
        .then(function(response){
            //get regularHours and save it into Array 'RegularHours'
            const info = response.data;
            const LENGTH = info.length;
        
            for (i = 0; i< LENGTH; i++) {
                regularHours.push(info[i].regularHours);
            }
            //console.log(regularHours);
        })
        .catch(function (error) {
            console.log(error);
        });
        console.log(jwtToken);   
        res.render("dashboard");
    });
*/

    server.get('/dashboard', (req, res, next) => {
        res.render("dashboard", {
            userRole: userRole
        });
    });
    server.get("/logout", (req, res, next)=>{
        res.render("logout", {
            userRole: userRole
        });
    });
    server.get('/payrollentry', (req, res, next) => {
        res.render("payrollentry", {
            userRole: userRole
        });
        console.log(userRole);
    });
    server.get('/edit', (req, res, next) => {
        const urlParams = new URLSearchParams(req.originalUrl);
        i= 0;
        for (const [key, value] of urlParams) {
            editFile[i] = value;
            i++;
        }
        res.render("edit", {
            userRole: userRole,
            editFile: editFile
        });
        editFile = {};    
    });
    server.get('/t4s', (req, res, next) => {
        res.render("t4s", {
            userRole: userRole
        });
        console.log(userRole);
    });
    server.get('/configure', (req, res, next) => {
        res.render("configure");
    });
    server.get("/datefilter", (req, res, next)=>{
        res.render("datefilter", {
            userRole: userRole
        });

    });
    server.get("/workinghours", (req, res, next)=>{
        res.render("workinghours", {
            userRole: userRole,
            editFile: editFile
        });
    });    
}


