const mongoose = require("mongoose");

const employeeInfo = new mongoose.Schema({
    email:{
        type:String
    },
    password:{
        type:String,
        required:true
    },
    jwtTokens:{
        type:String,
        required:true
    }
})

//collections

const User = new mongoose.model("User", employeeInfo);

module.exports = User;