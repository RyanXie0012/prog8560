// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const tabs = require ("./tabs.js");
const path = require ("path");
// See https://aka.ms/bot-services to learn more about the different parts of a bot.


//Express configure

const express = require("express");
const bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator");
const fileUpload = require('express-fileupload');
const session = require("express-session");

// Read botFilePath and botFileSecret from .env file.
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });
require("./db/conn");
const Register = require("./models/registers");
const static_path = path.join(__dirname, "./views");

// set up variables to use packages
const server = express();
server.use(fileUpload());
server.use(bodyParser.urlencoded({ extended: false }));
/*
const server = restify.createServer({
    formatters: {
        'text/html': function (req, res, body) {
            return body;
        },
    },
});
*/

// set up session
server.use(
    session({
      secret: "remebermeifyoucan",
      resave: false,
      saveUninitialized: true,
    })
);

// set path to public folders and view folders
server.set("views", path.join(__dirname, "views"));
server.use(express.static(static_path));
server.use(express.urlencoded({extended:false}));
server.use(express.json());
server.set("view engine", "ejs");

/*
server.get(
    '/*',
    restify.plugins.serveStatic({
        directory: __dirname + '/static',
    })
);


server.listen(process.env.port || process.env.PORT || 3333, function () {
    console.log(`\n${server.name} listening to ${server.url}`);
});
*/

server.listen(3333);
console.log("Listening to 3333...");
// Adding tabs to our app. This will setup routes to various views
tabs(server);

// Listen for incoming requests.
server.post('/api/messages', (req, res) => {
    adapter.processActivity(req, res, async (context) => {
        if (context.activity.type === ActivityTypes.Invoke)
            await messageExtension.run(context);
        else await bot.run(context);
    });
});
